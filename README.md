# ipabuilder
ipabuilder is a simple bash script to build a unsigned .ipa file from your .app file


### Installation
for installation you can simply just clone the repository and use the bash script
```bash
git clone https://gitlab.com/hsafaeim77/ipabuilder.git
cd ipabuilder
sudo mv ipabuilder /usr/local/bin
chmod +x /usr/local/bin/ipabuilder
```

### Usage
```bash
ipabuilder <path of .app file> <path to generate .ipa file>
```
